/*
 * Copyright (c) 2020, Egeniq
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.strimm.application.lib

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.recyclerview.widget.RecyclerView
import com.strimm.application.R
import com.strimm.application.lib.entity.ProgramGuideSchedule
import com.strimm.application.lib.item.ProgramGuideItemView
import com.strimm.application.utils.isLoadFirst
import kotlinx.coroutines.processNextEventInCurrentThread

class ProgramGuideListAdapter<T>(
    res: Resources,
    private val programGuideFragment: ProgramGuideHolder<T>,
    private val channelIndex: Int
) :
    RecyclerView.Adapter<ProgramGuideListAdapter.ProgramItemViewHolder<T>>(),
    ProgramGuideManager.Listener {

    private val programGuideManager: ProgramGuideManager<T>
    private val noInfoProgramTitle: String
    private val handler = Handler(Looper.getMainLooper())


    private var channelId: String = ""

    init {
        setHasStableIds(true)
        programGuideManager = programGuideFragment.programGuideManager
        noInfoProgramTitle = res.getString(R.string.title_no_program)
        onSchedulesUpdated()
    }

    override fun onTimeRangeUpdated() {
        // Do nothing
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onSchedulesUpdated() {
        val channel = programGuideManager.getChannel(channelIndex)
        if (channel != null) {
            channelId = channel.id
            notifyDataSetChanged()
        }
    }

    fun updateProgram(program: ProgramGuideSchedule<*>): Boolean {
        for (position in 0 until itemCount) {
            if (programGuideManager.getScheduleForChannelIdAndIndex(
                    channelId,
                    position
                ).id == program.id
            ) {
                notifyItemChanged(position)
                return true
            }
        }
        return false
    }

    override fun getItemCount(): Int {
        return programGuideManager.getSchedulesCount(channelId)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.programguide_item_program_container
    }

    override fun getItemId(position: Int): Long {
        return programGuideManager.getScheduleForChannelIdAndIndex(
            channelId,
            position
        ).startsAtMillis
    }

    override fun onBindViewHolder(holder: ProgramItemViewHolder<T>, position: Int) {
        val programGuideSchedule =
            programGuideManager.getScheduleForChannelIdAndIndex(channelId, position)
        val gapTitle = noInfoProgramTitle

        holder.onBind(programGuideSchedule, programGuideFragment, gapTitle,handler)
    }

    override fun onViewRecycled(holder: ProgramItemViewHolder<T>) {
        holder.onUnbind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramItemViewHolder<T> {
        val itemView = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return ProgramItemViewHolder(itemView)
    }

    class ProgramItemViewHolder<R>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var programGuideItemView: ProgramGuideItemView<R>? = null

        init {
            // Make all child view clip to the outline
            itemView.outlineProvider = ViewOutlineProvider.BACKGROUND
            itemView.clipToOutline = true
        }

        fun onBind(
            schedule: ProgramGuideSchedule<R>,
            programGuideHolder: ProgramGuideHolder<R>,
            gapTitle: String,
            handler: Handler
        ) {
            val programManager = programGuideHolder.programGuideManager
            programGuideItemView = itemView as ProgramGuideItemView<R>

            handler.postDelayed({
                // Code to be executed after the delay
                // This runs on the UI thread
                // latest
                if (!isLoadFirst) {
                    Log.e("SCHEDULE_FIRTS_PROGRAM", "onBindViewHolder: " + schedule)
                    programGuideItemView!!.performClick()
                    programGuideHolder.onScheduleClickedInternal(schedule)
                    isLoadFirst = true
                }
            }, 2000)

            programGuideItemView?.setOnClickListener {
                Log.e("PROGRAM_CLICK", "onBind: "+"IS_PROGRAMM_CLICK"+schedule.isGap)
                programGuideHolder.onScheduleClickedInternal(schedule)
            }

            programGuideItemView?.setValues(
                scheduleItem = schedule,
                fromUtcMillis = programManager.getFromUtcMillis(),
                toUtcMillis = programManager.getToUtcMillis(),
                gapTitle = gapTitle,
                displayProgress = programGuideHolder.DISPLAY_SHOW_PROGRESS
            )
        }

        fun onUnbind() {
            programGuideItemView?.setOnClickListener(null)
            programGuideItemView?.clearValues()
        }
    }
}